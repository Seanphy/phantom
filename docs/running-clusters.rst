Running on a cluster
====================

Here are some instructions for running Phantom on particular clusters.

Contents
--------

.. toctree::
   :maxdepth: 1

   ozstar
   monarch
   pawsey
   raijin
   DiAL
   g2
