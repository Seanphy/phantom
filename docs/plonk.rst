Plonk
=====

Plonk is a Python package for analysis and visualisation of smoothed particle hydrodynamics data. It is open source.

It uses Splash for visualisation.

- Docs: https://plonk.readthedocs.io/
- Repo: https://www.github.com/dmentipl/plonk
